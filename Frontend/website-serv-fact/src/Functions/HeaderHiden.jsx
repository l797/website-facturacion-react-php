import '../css/headerHiden.css';
import {isMobile} from '../Functions/isMobile';

// Componente para ocultar y mostrar el header al hacer scroll
export default function HeaderHiden() {

    let headerInf = document.getElementsByClassName('header');
    let elementsTop = document.getElementsByClassName('top');

    // evento scroll
    window.addEventListener("scroll", function() {
        // donde nos encontramos actualmente
        let desplazamientoActual = window.pageYOffset;

        for( var i = 0; i < headerInf.length; i++) {
            let header = headerInf[i];
            let top = elementsTop[i];
            // ocultar o mostrar el menu superior negro
            if(desplazamientoActual > 35 && !isMobile()) {
                header.classList.add('mostrarMenu');
                top.classList.add('masTop');
                
            } else {
                header.classList.remove('mostrarMenu');
                top.classList.remove('masTop');
            }
        }
    });

}








