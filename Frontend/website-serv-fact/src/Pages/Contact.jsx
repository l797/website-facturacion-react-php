import Wsp from '../Components/Wsp';
import Header from '../Components/Header';
import Footer from '../Components/Footer';
import Contacto from '../Components/Contacto';
import GoTop from '../Functions/GoTop';
import SliderNav from '../Components/SliderNav';
import AnimationScroll from '../Functions/AnimationScroll';
import HeaderHiden from '../Functions/HeaderHiden';
import {useState, useEffect} from 'react';
import Loading from '../Components/Loading';


function Contact() {
  const [loading, setLoading] = useState(true);
  //Mostrar el loading
  const mostrarCarga = ()=>{
    setTimeout(()=>{
    setLoading(false);
    }, 1000);
  }
  useEffect (()=> {
    mostrarCarga();
  },[])

  return (
    <>
      { loading ? <Loading/>:
        <> 
          < HeaderHiden />
          < AnimationScroll />
          < Wsp />
          < Header />
          < SliderNav titleSliderNav="Contáctanos"/>
          < Contacto />
          < Footer/>
          < GoTop />
        </>
      }
    </>
  );
}

export default Contact;