import Wsp from '../Components/Wsp';
import Header from '../Components/Header';
import Hero from '../Components/Hero';
import Nosotros from '../Components/Nosotros';
import Precios from '../Components/Precios';
import Footer from '../Components/Footer';
import Contacto from '../Components/Contacto';
import Ventajas from '../Components/Ventajas';
import Clientes from '../Components/Clientes';
import GoTop from '../Functions/GoTop';
import AnimationScroll from '../Functions/AnimationScroll';
import HeaderHiden from '../Functions/HeaderHiden';
import Loading from '../Components/Loading';
import {useState, useEffect} from 'react';


function Inicio() {
  const [loading, setLoading] = useState(true);
  //Mostrar el loading
  const mostrarCarga = ()=>{
    setTimeout(()=>{
    setLoading(false);
    }, 1000);
  }
  useEffect (()=> {
    mostrarCarga();
  },[])

  return (
    <>
      { loading ? <Loading/>: 
        <>
          < AnimationScroll />
          < HeaderHiden />
          < Wsp />
          < Header />
          < Hero />
          < Nosotros />
          < Ventajas />
          < Clientes />
          < Precios/>
          < Contacto />
          < Footer/>
          < GoTop />
        </>
      }
    </>
  );
}

export default Inicio;