import Wsp from '../Components/Wsp';
import Header from '../Components/Header';
import Footer from '../Components/Footer';
import GoTop from '../Functions/GoTop';
import Precios from '../Components/Precios';
import SliderNav from '../Components/SliderNav';
import AnimationScroll from '../Functions/AnimationScroll';
import HeaderHiden from '../Functions/HeaderHiden';
import Loading from '../Components/Loading';
import {useState, useEffect} from 'react';


function Prices() {
  const [loading, setLoading] = useState(true);
  //Mostrar el loading
  const mostrarCarga = ()=>{
    setTimeout(()=>{
    setLoading(false);
    }, 1000);
  }
  useEffect (()=> {
    mostrarCarga();
  },[])

  return (
    <>
      { loading ? <Loading/>: 
        <>
          < HeaderHiden />
          < AnimationScroll />
          < Wsp />
          < Header />
          < SliderNav  titleSliderNav="Precios"/>
          < Precios />
          < Footer/>
          < GoTop />
        </>
      }
    </>
  );
}

export default Prices;