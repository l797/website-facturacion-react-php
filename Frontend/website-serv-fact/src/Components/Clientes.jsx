import '../css/clientes.css';
import  cosmos from  '../assets/clientes/cosmos.png';
import  doil from  '../assets/clientes/doil.png';
import  etours from  '../assets/clientes/etours.png';
import  tjomax from  '../assets/clientes/tjomax.png';
import  ucv from  '../assets/clientes/ucv.png';

import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';


//Componente funcional Clientes
function Clientes (){
    const responsive = {
        0: { items: 1 },
        450: { items: 2 },
        750: { items: 3 },
        1070: { items: 4 },
        1250: { items: 5 },
    };
    
    const handleDragStart = (e) => e.preventDefault();

    const items = [
        <img src={cosmos} onDragStart={handleDragStart} role="presentation" alt='cosmos' key={1}/>,
        <img src={doil} onDragStart={handleDragStart} role="presentation" alt='Doil' key={2}/>,
        <img src={etours} onDragStart={handleDragStart} role="presentation" alt='eTours' key={3}/>,
        <img src={tjomax} onDragStart={handleDragStart} role="presentation" alt='TJOMAX' key={4}/>,
        <img src={ucv} onDragStart={handleDragStart} role="presentation" alt='UCV' key={5}/>,
    ];

    return (  
        <div className='clientes'>
            <div className='clientes_container'>
                <div className='clientes_title'>
                    <h2>Nuestros clientes nos respaldan</h2>
                </div>
                <div className='clientes_sponsors'>
                    <AliceCarousel
                        infinite
                        autoPlay={true}
                        autoPlayInterval={2000}
                        disableButtonsControls={true}
                        disableDotsControls={true}
                        mouseTracking
                        items={items}
                        responsive={responsive}
                    />
                </div>
            </div>
        </div>
    );
  
}

export default Clientes;