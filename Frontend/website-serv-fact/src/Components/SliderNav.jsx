import '../css/slidernav.css';
import PropTypes from 'prop-types';

function SliderNav({titleSliderNav}) {
  return (
    <div className='sliderNav top'>
      <div className='sliderNav_container'>
        <div className='sliderNav_text'>
          <h2>{titleSliderNav}</h2>
        </div>
      </div>
    </div>
  );
}

SliderNav.propTypes = {
  titleSliderNav: PropTypes.string
}

export default SliderNav;