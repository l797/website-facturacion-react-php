<?php

    class ConexionBD {

        public function obtenerVariablesEnv($key) {
            if (defined("_ENV_CACHE")) {
                $vars = _ENV_CACHE;
            }else{
                $file = "../config/env.php";
                if (!file_exists($file)) {
                    throw new Exception("El archivo de variables de entorno ($file) no existe. Por favor crearlo");
                }
                $vars = parse_ini_file($file);
                define("_ENV_CACHE",$vars);
            }
        
            if (isset($vars[$key])) {
                return $vars[$key];
            } else {
                throw new Exception("La clave especificada (" . $key . ") no existe en el archivo ENV");
            }
        }
        
        public function obtenerConexion() {
            $password = $this->obtenerVariablesEnv("MYSQL_PASSWORD");
            $user = $this->obtenerVariablesEnv("MYSQL_USER");
            $dbName = $this->obtenerVariablesEnv("MYSQL_DATABASE_NAME");
            $host = $this->obtenerVariablesEnv("MYSQL_LOCALHOST");
            $database = new PDO('mysql:host=' . $host.';dbname=' . $dbName, $user, $password);
            $database->query("set names utf8;");
            $database->setAttribute(PDO::ATTR_EMULATE_PREPARES, FALSE);
            $database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $database->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
            return $database;
        }
    }
?>