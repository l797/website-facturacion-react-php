<?php

    include_once "conexionBD.php";

    class Contacto {

        function eliminarContacto($id) {
            $conexion = new ConexionBD();
            $bd = $conexion->obtenerConexion();
            $sentencia = $bd->prepare("DELETE FROM contactos WHERE id_contacto = ?");
            return $sentencia->execute([$id]);
        }
        
        function actualizarContacto($contacto) {
            $conexion = new ConexionBD();
            $bd = $conexion->obtenerConexion();
            $sentencia = $bd->prepare("UPDATE contactos SET nombre = ?, email = ?, telefono = ?, asunto = ? WHERE id_contacto = ?");
            return $sentencia->execute([$contacto->nombre, $contacto->email, $contacto->telefono, $contacto->asunto, $contacto->id]);
        }
        
        function obtenerContactoPorId($id) {
            $conexion = new ConexionBD();
            $bd = $conexion->obtenerConexion();
            $sentencia = $bd->prepare("SELECT id_contacto, nombre, email, telefono, asunto, fecha_captura FROM contactos WHERE id_contacto = ?");
            $sentencia->execute([$id]);
            return $sentencia->fetchObject();
        }
        
        function obtenerContactos() {
            $conexion = new ConexionBD();
            $bd = $conexion->obtenerConexion();
            $sentencia = $bd->query("SELECT id_contacto, nombre, email, telefono, asunto, fecha_captura FROM contactos");
            return $sentencia->fetchAll();
        }
        
        function guardarContacto($contacto) {
            $conexion = new ConexionBD();
            $bd = $conexion->obtenerConexion();
            $fecha_captura = date('Y-m-d');
            $sentencia = $bd->prepare("INSERT INTO contactos(nombre, email, telefono, asunto, fecha_captura) VALUES (?, ?, ?, ?,'$fecha_captura')");
            return $sentencia->execute([$contacto->nombre, $contacto->email, $contacto->telefono, $contacto->asunto]);
        } 

    }

?>