<?php
    include_once "../config/cors.php";
    include_once "../model/contacto.php";
    
    $obj = new Contacto();
    $contactos = $obj->obtenerContactos();
    echo json_encode($contactos);
?>