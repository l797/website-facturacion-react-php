<?php
    include_once "../config/cors.php";
    include_once "../model/contacto.php";
    
    if (!isset($_GET["id"])) {
        echo json_encode(null);
        exit;
    }

    $id = $_GET["id"];
    $obj = new Contacto();
    $contacto = $obj->obtenerContactoPorId($id);
    echo json_encode($contacto);
?>