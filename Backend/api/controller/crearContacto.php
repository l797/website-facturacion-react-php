<?php
    include_once "../config/cors.php";
    include_once "../model/contacto.php";
    
    $obj = new Contacto();
    $contacto = json_decode(file_get_contents("php://input"));
    $resultado = $obj->guardarContacto($contacto);
    echo json_encode($resultado);
?>